$(document).ready(function(){

// masonry
    /*!
         * imagesLoaded PACKAGED v4.1.3
         * JavaScript is all like "You images are done yet or what?"
         * MIT License
         */

        !function(e,t){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",t):"object"==typeof module&&module.exports?module.exports=t():e.EvEmitter=t()}("undefined"!=typeof window?window:this,function(){function e(){}var t=e.prototype;return t.on=function(e,t){if(e&&t){var i=this._events=this._events||{},n=i[e]=i[e]||[];return-1==n.indexOf(t)&&n.push(t),this}},t.once=function(e,t){if(e&&t){this.on(e,t);var i=this._onceEvents=this._onceEvents||{},n=i[e]=i[e]||{};return n[t]=!0,this}},t.off=function(e,t){var i=this._events&&this._events[e];if(i&&i.length){var n=i.indexOf(t);return-1!=n&&i.splice(n,1),this}},t.emitEvent=function(e,t){var i=this._events&&this._events[e];if(i&&i.length){var n=0,o=i[n];t=t||[];for(var r=this._onceEvents&&this._onceEvents[e];o;){var s=r&&r[o];s&&(this.off(e,o),delete r[o]),o.apply(this,t),n+=s?0:1,o=i[n]}return this}},t.allOff=t.removeAllListeners=function(){delete this._events,delete this._onceEvents},e}),function(e,t){"use strict";"function"==typeof define&&define.amd?define(["ev-emitter/ev-emitter"],function(i){return t(e,i)}):"object"==typeof module&&module.exports?module.exports=t(e,require("ev-emitter")):e.imagesLoaded=t(e,e.EvEmitter)}("undefined"!=typeof window?window:this,function(e,t){function i(e,t){for(var i in t)e[i]=t[i];return e}function n(e){var t=[];if(Array.isArray(e))t=e;else if("number"==typeof e.length)for(var i=0;i<e.length;i++)t.push(e[i]);else t.push(e);return t}function o(e,t,r){return this instanceof o?("string"==typeof e&&(e=document.querySelectorAll(e)),this.elements=n(e),this.options=i({},this.options),"function"==typeof t?r=t:i(this.options,t),r&&this.on("always",r),this.getImages(),h&&(this.jqDeferred=new h.Deferred),void setTimeout(function(){this.check()}.bind(this))):new o(e,t,r)}function r(e){this.img=e}function s(e,t){this.url=e,this.element=t,this.img=new Image}var h=e.jQuery,a=e.console;o.prototype=Object.create(t.prototype),o.prototype.options={},o.prototype.getImages=function(){this.images=[],this.elements.forEach(this.addElementImages,this)},o.prototype.addElementImages=function(e){"IMG"==e.nodeName&&this.addImage(e),this.options.background===!0&&this.addElementBackgroundImages(e);var t=e.nodeType;if(t&&d[t]){for(var i=e.querySelectorAll("img"),n=0;n<i.length;n++){var o=i[n];this.addImage(o)}if("string"==typeof this.options.background){var r=e.querySelectorAll(this.options.background);for(n=0;n<r.length;n++){var s=r[n];this.addElementBackgroundImages(s)}}}};var d={1:!0,9:!0,11:!0};return o.prototype.addElementBackgroundImages=function(e){var t=getComputedStyle(e);if(t)for(var i=/url\((['"])?(.*?)\1\)/gi,n=i.exec(t.backgroundImage);null!==n;){var o=n&&n[2];o&&this.addBackground(o,e),n=i.exec(t.backgroundImage)}},o.prototype.addImage=function(e){var t=new r(e);this.images.push(t)},o.prototype.addBackground=function(e,t){var i=new s(e,t);this.images.push(i)},o.prototype.check=function(){function e(e,i,n){setTimeout(function(){t.progress(e,i,n)})}var t=this;return this.progressedCount=0,this.hasAnyBroken=!1,this.images.length?void this.images.forEach(function(t){t.once("progress",e),t.check()}):void this.complete()},o.prototype.progress=function(e,t,i){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!e.isLoaded,this.emitEvent("progress",[this,e,t]),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,e),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&a&&a.log("progress: "+i,e,t)},o.prototype.complete=function(){var e=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emitEvent(e,[this]),this.emitEvent("always",[this]),this.jqDeferred){var t=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[t](this)}},r.prototype=Object.create(t.prototype),r.prototype.check=function(){var e=this.getIsImageComplete();return e?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,this.proxyImage.addEventListener("load",this),this.proxyImage.addEventListener("error",this),this.img.addEventListener("load",this),this.img.addEventListener("error",this),void(this.proxyImage.src=this.img.src))},r.prototype.getIsImageComplete=function(){return this.img.complete&&void 0!==this.img.naturalWidth},r.prototype.confirm=function(e,t){this.isLoaded=e,this.emitEvent("progress",[this,this.img,t])},r.prototype.handleEvent=function(e){var t="on"+e.type;this[t]&&this[t](e)},r.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},r.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},r.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this),this.proxyImage.removeEventListener("error",this),this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype=Object.create(r.prototype),s.prototype.check=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.img.src=this.url;var e=this.getIsImageComplete();e&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},s.prototype.unbindEvents=function(){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype.confirm=function(e,t){this.isLoaded=e,this.emitEvent("progress",[this,this.element,t])},o.makeJQueryPlugin=function(t){t=t||e.jQuery,t&&(h=t,h.fn.imagesLoaded=function(e,t){var i=new o(this,e,t);return i.jqDeferred.promise(h(this))})},o.makeJQueryPlugin(),o});



        var $container = $('.masonry');
            $container.imagesLoaded(function(){
                $container.masonry({
                    itemSelector : '.masonry__item',
                    columnWidth : '.masonry__item_sizer',
                    // gutterWidth: 10,
                });
            });
//smoothscroll

    $(document).on("scroll", onScroll);
    var ancor = $('#nav a');
    $(function() {
        ancor.click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
                && location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 125 //offsets for fixed header
                    }, 1000);
                    return false;
                }
            }
        });
        //Executed on page load with URL containing an anchor tag.
        if($(location.href.split("#")[1])) {
            var target = $('#'+location.href.split("#")[1]);
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 125 //offset height of header here too.
                }, 1000);
                return false;
            }
        }
    });

   
    function onScroll(event){
        var scrollPos = $(document).scrollTop();
        ancor.each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top - 124 <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                ancor.removeClass("active");
                currLink.addClass("active");
            }
            else{
                currLink.removeClass("active");
            }
        });
    }

    $(function() {
        var header = $("header");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 500) {
                header.addClass("fixed animated slideInDown");
            } else {

                header.removeClass("fixed animated slideInDown");
            }
        });
    });
// floating placeholder
 $("input, textarea").each(function(e) {
          $(this).wrap('<fieldset></fieldset>');
          var tag = $(this).attr("placeholder");
          //var tag= $(this).data("tag");
          $(this).attr("placeholder", "");
          $(this).after('<label for="name">' + tag + '</label>');
        });

        $('input, textarea').on('blur', function() {
          if (!$(this).val() == "") {
            $(this).next().addClass('stay');
          } else {
            $(this).next().removeClass('stay');
          }
        });

//hamburger
    $('#hamburger').click(function(){
        $(this).toggleClass('open');

    });

    $('main').click(function(){
        $('#hamburger').removeClass('open');
        $('#nav').removeClass('in');
    });

    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span>&rang;</span></div>',
        prevArrow: '<div class="slick-arrow-left"><span>&lang;</span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 3,
            //     slidesToScroll: 3,
            //     infinite: true,
            //     dots: true
            //   }
            // },
            // {
            //   breakpoint: 600,
            //   settings: {
            //     slidesToShow: 2,
            //     slidesToScroll: 2
            //   }
            // },
            // {
            //   breakpoint: 480,
            //   settings: {
            //     slidesToShow: 1,
            //     slidesToScroll: 1
            //   }
            // }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });
//animate css
$(window).on('load', function(){
        if ($(window).width() > 1023) {

            $('.jsAppendLeft').addClass("a-hidden").viewportChecker({
                classToAdd: 'a-visible animated slideInLeft',
                offset: 30
            });

            $('.jsAppendRight').addClass("a-hidden").viewportChecker({
                classToAdd: 'a-visible animated slideInRight',
                offset: 30
            });

            $('.jsAppendUp').addClass("a-hidden").viewportChecker({
                classToAdd: 'a-visible animated slideInUp',
                offset: 30
            });

        }
    });



// $(window).scroll(function () {
//     var bottom_of_window = $(window).scrollTop();
//     $('.b_rehabilitation_center ._center_item').each(function (i) {
//         var  bottom_of_object = $(this).offset().top - $(this).outerHeight() * 3;
//         if (bottom_of_window > bottom_of_object) {
//           $(this).addClass('animated bounceInUp');
//         }
//     });
//   });
// autoheight textarea
    (function($)
{
    /**
     * Auto-growing textareas; technique ripped from Facebook
     *
     *
     * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function(options)
    {
        return this.filter('textarea').each(function()
        {
            var self         = this;
            var $self        = $(self);
            var minHeight    = $self.height();
            var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
            var settings = $.extend({
                preGrowCallback: null,
                postGrowCallback: null
              }, options );

            var shadow = $('<div></div>').css({
                position:    'absolute',
                top:         -10000,
                left:        -10000,
                width:       $self.width(),
                fontSize:    $self.css('fontSize'),
                fontFamily:  $self.css('fontFamily'),
                fontWeight:  $self.css('fontWeight'),
                lineHeight:  $self.css('lineHeight'),
                resize:      'none',
                'word-wrap': 'break-word'
            }).appendTo(document.body);

            var update = function(event)
            {
                var times = function(string, number)
                {
                    for (var i=0, r=''; i<number; i++) r += string;
                    return r;
                };

                var val = self.value.replace(/&/g, '&amp;')
                                    .replace(/</g, '&lt;')
                                    .replace(/>/g, '&gt;')
                                    .replace(/\n$/, '<br/>&#xa0;')
                                    .replace(/\n/g, '<br/>')
                                    .replace(/ {2,}/g, function(space){ return times('&#xa0;', space.length - 1) + ' ' });

                // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
                if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
                    val += '<br />';
                }

                shadow.css('width', $self.width());
                shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.

                var newHeight=Math.max(shadow.height() + noFlickerPad, minHeight);
                if(settings.preGrowCallback!=null){
                  newHeight=settings.preGrowCallback($self,shadow,newHeight,minHeight);
                }

                $self.height(newHeight);

                if(settings.postGrowCallback!=null){
                  settings.postGrowCallback($self);
                }
            }

            $self.change(update).keyup(update).keydown({event:'keydown'},update);
            $(window).resize(update);

            update();
        });
    };
})(jQuery);

// slick slide same height
    var stHeight = $('#teachers .slick-track').height();
    $('#teachers .slick-slide').css('height',stHeight + 'px' );

    var stHeight = $('#feedbacks .slick-track').height();
    $('#feedbacks .slick-slide').css('height',stHeight + 'px' );


// phone mask
    $(function() {
        $('[name="phone"]').mask("+7(000)000-00-00", {
            clearIfNotMatch: true,
            placeholder: "+7(___)___-__-__"
        });
        $('[name="phone"]').focus(function(e) {
            if ($('[name="phone"]').val().length == 0) {
                $(this).val('+7(');
            }
        })
    });
    
    
//equal hieght
    ;( function( $, window, document, undefined )
    {
        'use strict';

        var $list       = $( '.achievement' ),
            $items      = $list.find( '.achievement__item' ),
            setHeights  = function()
            {
                $items.css( 'height', 'auto' );

                var perRow = Math.floor( $list.width() / $items.width() );
                if( perRow == null || perRow < 2 ) return true;

                for( var i = 0, j = $items.length; i < j; i += perRow )
                {
                    var maxHeight   = 0,
                        $row        = $items.slice( i, i + perRow );

                    $row.each( function()
                    {
                        var itemHeight = parseInt( $( this ).outerHeight() );
                        if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                    });
                    $row.css( 'height', maxHeight );
                }
            };

        setHeights();
        $( window ).on( 'resize', setHeights );
        $list.find( 'img' ).on( 'load', setHeights );
    })( jQuery, window, document );
    



// current month
    var months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
    var date = new Date();

    document.getElementById('promoMonth').innerHTML = months[date.getMonth()];


//form styler
    //select
    (function($) {
        $(function() {
            $('input, select').styler({

            });
        });
    })(jQuery);

//form validation
    // send form
    $('#messageForm1 .butn, #messageForm2 .butn, #messageForm3 .butn').click(function () {
        var parentClass = $(this).attr('rel');
        validate = 1;
        validate_msg = '';
        form = $('#' + $(this).attr('data-rel'));
        jQuery.each(form.find('.form-group input'), function (key, value) {

            if ($(this).val() == '') {
                validate_msg += $(this).attr('title') + '\n';
                validate = 0;
                $(this).focus();

                $(this).addClass('red_input');

                $(this).keyup(function () {

                    $(this).removeClass('red_input');

                });
            }

            else {
                $(this).removeClass('red_input');
            }
        });

        if (validate == 1) {
            $.ajax({
                url: 'send.php'
                , data: 'action=send_form&' + form.serialize()
                , success: function (data) {
                    $('form').trigger('reset');
                    // $('#call-back, #request').modal('hide');
                    $('#thanks').modal('show');
                }
            });
        }
        else {}
    });

    $(function(){
  
    $(document).click(function (e) {
        var div = $('._social ._link'),
            div2 = $('._social ._all_link');
        if ( !div.is(e.target) && div.has(e.target).length === 0 ) {
            div2.hide();
        }
    });
  
});









});
